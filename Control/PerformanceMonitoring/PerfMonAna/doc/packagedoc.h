/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

/**
@page PerfMonAna_page PerfMonAna Package

@section PerfMonAna_PerfMonAnaIntro Introduction

This package contains python classes and python script to analyze tuple files produced by the PerfMonComps package (actually the @c PerfMonMTSvc and its tools).

*/
