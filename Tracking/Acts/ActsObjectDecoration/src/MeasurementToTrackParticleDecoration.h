/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

///////////////////////////////////////////////////////////////////
// Header file for class MeasurementToTrackParticleDecoration
//
// The algorithm extends xAOD::TrackParticles
// with additional decorations associated to measurements,
// i.e. residuals, pulls, modules
//
///////////////////////////////////////////////////////////////////

#ifndef MEASUREMENTTOTRACKPARTICLEDECORATION_H
#define MEASUREMENTTOTRACKPARTICLEDECORATION_H

#include "AthenaBaseComps/AthReentrantAlgorithm.h"
#include "GaudiKernel/ServiceHandle.h"
#include "StoreGate/ReadHandleKey.h"

#include "StoreGate/WriteDecorHandleKey.h"
#include "StoreGate/WriteDecorHandle.h"

#include "ActsGeometryInterfaces/IActsTrackingGeometryTool.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "Acts/Definitions/Units.hpp"
#include "ActsEvent/TrackContainer.h"
#include "Acts/EventData/TrackStateProxy.hpp"

using namespace Acts::UnitLiterals;
using EffectiveCalibrated =
  typename Acts::detail_lt::DynamicSizeTypes<>::CoefficientsMap;
using EffectiveCalibratedCovariance =
  typename Acts::detail_lt::DynamicSizeTypes<>::CovarianceMap;

namespace ActsTrk {

    class MeasurementToTrackParticleDecoration : public AthReentrantAlgorithm  {
    public:
        MeasurementToTrackParticleDecoration(const std::string &name,ISvcLocator *pSvcLocator);
        virtual ~MeasurementToTrackParticleDecoration() = default;

        virtual StatusCode initialize() override;
        virtual StatusCode execute(const EventContext& ctx) const override;
        virtual StatusCode finalize() override { return StatusCode::SUCCESS;};

        enum Subdetector {
            INVALID_DETECTOR=-1, INNERMOST_PIXEL, PIXEL, STRIP, N_SUBDETECTORS
        };
        enum Region {
            INVALID_REGION=-1, BARREL, ENDCAP
        };
        enum MeasurementType {
	    INVALID_MEASUREMENT=-1, HIT, OUTLIER, HOLE, BIASED, UNBIASED
        };

      std::pair<Acts::BoundVector, Acts::BoundMatrix> getUnbiasedTrackParameters(const typename ActsTrk::TrackStateBackend::ConstTrackStateProxy &state, bool useSmoothed=true) const;

      float evaluatePull(const float residual,
			 const float measurementCovariance,
			 const float trackParameterCovariance,
			 const bool evaluateUnbiased) const;
      
    private:
      
      ToolHandle<IActsTrackingGeometryTool> m_trackingGeometryTool{this, "TrackingGeometryTool", ""};
      
      SG::ReadHandleKey<xAOD::TrackParticleContainer> m_trackParticlesKey {
	this, "TrackParticleKey", "InDetTrackParticles", "Input track particle collection"};
      
      SG::WriteDecorHandleKey<xAOD::TrackParticleContainer> m_measurementRegionKey{
	this, "MeasurementRegionKey", m_trackParticlesKey,"measurement_region",
	"Decorate track particle with region of the measurement (barrel, ec)"};
      SG::WriteDecorHandleKey<xAOD::TrackParticleContainer> m_measurementDetectorKey{
	this, "MeasurementDetectorKey", m_trackParticlesKey,"measurement_det",
	"Decorate track particle with measurement detector id (innermost pix, pix, strip)"};
      SG::WriteDecorHandleKey<xAOD::TrackParticleContainer> m_measurementLayerKey{
	this, "MeasurementLayerKey", m_trackParticlesKey,"measurement_iLayer",
	"Decorate track particle with measurement layer"};
      SG::WriteDecorHandleKey<xAOD::TrackParticleContainer> m_measurementTypeKey{
	this, "MeasurementTypeKey", m_trackParticlesKey,"measurement_type",
	"Decorate track particle with type of track state (outlier,hole, biased/unbiased)"};
      SG::WriteDecorHandleKey<xAOD::TrackParticleContainer> m_measurementPhiWidthKey{
	this, "MeasurementPhiWidthKey", m_trackParticlesKey,"hitResiduals_phiWidth",
	"Decorate track particle with measurement cluster size (in r-phi)"};
      SG::WriteDecorHandleKey<xAOD::TrackParticleContainer> m_measurementEtaWidthKey{
	this, "MeasurementEtaWidthKey", m_trackParticlesKey,"hitResiduals_etaWidth",
	"Decorate track particle with measurement cluster size (in eta)"};
      
      
      SG::WriteDecorHandleKey<xAOD::TrackParticleContainer> m_residualLocXkey{
	this, "ResidualLocXkey", m_trackParticlesKey,"hitResiduals_residualLocX",
	"Decorate track particle with unbiased residual in local x"};
      SG::WriteDecorHandleKey<xAOD::TrackParticleContainer> m_pullLocXkey{
	this, "PullLocXkey", m_trackParticlesKey,"hitResiduals_pullLocX",
	"Decorate track particle with unbiased pull in local x"};
      SG::WriteDecorHandleKey<xAOD::TrackParticleContainer> m_measurementLocXkey{
	this, "MeasurementLocXkey", m_trackParticlesKey,"measurementLocX",
	"Decorate track particle with measurement local x"};
      SG::WriteDecorHandleKey<xAOD::TrackParticleContainer> m_trackParameterLocXkey{
	this, "TrackParameterLocXkey", m_trackParticlesKey,"trackParamLocX",
	"Decorate track particle with unbiased prediction in local x"};
      SG::WriteDecorHandleKey<xAOD::TrackParticleContainer> m_measurementLocCovXkey{
	this, "MeasurementLocCovXkey", m_trackParticlesKey,"measurementLocCovX",
	"Decorate track particle with local x measurement covariance"};
      SG::WriteDecorHandleKey<xAOD::TrackParticleContainer> m_trackParameterLocCovXkey{
	this, "TrackParameterLocCovXkey", m_trackParticlesKey,"trackParameterLocCovX",
	"Decorate track particle with unbiased local x prediction covariance"};
      
      SG::WriteDecorHandleKey<xAOD::TrackParticleContainer> m_residualLocYkey{
	this, "ResidualLocYkey", m_trackParticlesKey,"hitResiduals_residualLocY",
	"Decorate track particle with unbiased residual in local y"};
      SG::WriteDecorHandleKey<xAOD::TrackParticleContainer> m_pullLocYkey{
	this, "PullLocYkey", m_trackParticlesKey,"hitResiduals_pullLocY",
	"Decorate track particle with unbiased pull in local y"};
      SG::WriteDecorHandleKey<xAOD::TrackParticleContainer> m_measurementLocYkey{
	this, "MeasurementLocYkey", m_trackParticlesKey,"measurementLocY",
	"Decorate track particle with measurement local y"};
      SG::WriteDecorHandleKey<xAOD::TrackParticleContainer> m_trackParameterLocYkey{
	this, "TrackParameterLocYkey", m_trackParticlesKey,"trackParamLocY",
	"Decorate track particle with unbiased prediction in local y"};
      SG::WriteDecorHandleKey<xAOD::TrackParticleContainer> m_measurementLocCovYkey{
	this, "MeasurementLocCovYkey", m_trackParticlesKey,"measurementLocCovY",
	"Decorate track particle with local y measurement covariance"};
      SG::WriteDecorHandleKey<xAOD::TrackParticleContainer> m_trackParameterLocCovYkey{
	this, "TrackParameterLocCovYkey", m_trackParticlesKey,"trackParameterLocCovY",
	"Decorate track particle with unbiased local y prediction covariance"};
    };
}

#endif // MEASUREMENTTOTRACKPARTICLEDECORATION_H

